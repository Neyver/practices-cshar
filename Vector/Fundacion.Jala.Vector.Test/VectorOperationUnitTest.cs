namespace Fundacion.Jala.Vector.Test
{
    using System;
    using Fundacion.Jala.Vector.BusinessLogic;
    using Fundacion.Jala.Vector.BusinessLogic.Model;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class VectorOperationUnitTest
    {
        #region Positive Cases
        [TestMethod]
        public void Given_TwoVectors_When_Sum_Then_Expected_A_Vector_With_TheSum()
        {
            var vectorA = new TransformVector("2,1");
            var vectorB = new TransformVector("-3,2");
            IOperation operation = new VectorOperations();
            var resSum = operation.Add(vectorA.Vector, vectorB.Vector);

            Assert.AreEqual(resSum.PositionX, -1);
            Assert.AreEqual(resSum.PositionY, 3);
        }

        [TestMethod]
        public void Given_TwoVectors_When_Sum_Numbers_Negatives_Then_Expected_A_Vector_With_Numbers_negatives()
        {
            var vectorA = new TransformVector("-2,-1");
            var vectorB = new TransformVector("-3,-2");
            IOperation operation = new VectorOperations();
            var resSum = operation.Add(vectorA.Vector, vectorB.Vector);

            Assert.AreEqual(resSum.PositionX, -5);
            Assert.AreEqual(resSum.PositionY, -3);
        }

        [TestMethod]
        public void Given_TwoVector_When_DoProduct_Then_compare_with_Number_Err_ThenExpected_Number_Of_the_Product()
        {
            var vectorA = new TransformVector("2,1");
            var vectorB = new TransformVector("5,3");
            IOperation operation = new VectorOperations();
            var resDoProduct = operation.DotProduct(vectorA.Vector, vectorB.Vector);

            var expected = 45;
            Assert.AreNotEqual(resDoProduct, expected);
        }

        [TestMethod]
        public void Given_TwoVector_When_Length_Expected_Number_Of_the_Operation()
        {
            var vector = new TransformVector("2,2");
            IOperation operation = new VectorOperations();
            var resLength = operation.Length(vector.Vector);
            Assert.AreEqual(resLength, Math.Sqrt(8));
        }

        [TestMethod]
        public void Given_TwoVector_When_Distance_Expected_Number_Of_the_Operation()
        {
            var vectorA = new TransformVector("2,1");
            var vectorB = new TransformVector("-3,2");
            IOperation operation = new VectorOperations();
            var resDistance = operation.Distance(vectorA.Vector, vectorB.Vector);

            var expected = Math.Sqrt(26);
            Assert.AreEqual(resDistance, expected);
        }
        #endregion
        #region negative cases
        [TestMethod]
        public void Given_TwoVector_When_DoProduct_Expected_Number_Of_the_Product()
        {
            var vectorA = new TransformVector("2,1");
            var vectorB = new TransformVector("5,3");
            IOperation operation = new VectorOperations();
            var resDoProduct = operation.DotProduct(vectorA.Vector, vectorB.Vector);

            var expected = 13;
            Assert.AreEqual(resDoProduct, expected);
        }

        [TestMethod]
        public void Given_TwoVectors_When_Sum_Then_Verify_With_Others_Values_Then_Expected_Not_Equals()
        {
            var vectorA = new TransformVector("2,1");
            var vectorB = new TransformVector("-3,2");
            IOperation operation = new VectorOperations();
            var resSum = operation.Add(vectorA.Vector, vectorB.Vector);

            Assert.AreNotEqual(resSum.PositionX, 4);
            Assert.AreNotEqual(resSum.PositionY, 4);
        }
        #endregion
    }
}
