﻿//-----------------------------------------------------------------------
// <copyright file="VectorOperations.cs" company="Fundacion Jala">
//     Copyright (c) Fundacion Jala. All rights reserved.
// </copyright>
// <author>Neyvert</author>
//-----------------------------------------------------------------------
namespace Fundacion.Jala.Vector.BusinessLogic
{
    using System;
    using Fundacion.Jala.Vector.BusinessLogic.Model;

    /// <summary>
    /// Let create a Vector Operations
    /// </summary>
    public class VectorOperations : IOperation
    {
        /// <summary>
        /// Method That Sum two vectors
        /// </summary>
        /// <param name="vectorA">First is a Vector</param>
        /// <param name="vectorB">Second is a Vector</param>
        /// <returns>A vector with the Sum</returns>
        public Vector Add(Vector vectorA, Vector vectorB)
        {
            var resVector = new Vector(vectorA.PositionX + vectorB.PositionX, vectorA.PositionY + vectorB.PositionY);
            return resVector;
        }

        /// <summary>
        /// Method That Dot Product two vectors
        /// </summary>
        /// <param name="vectorA">First is a Vector</param>
        /// <param name="vectorB">Second is a Vector</param>
        /// <returns>A integer with the Product</returns>
        public int DotProduct(Vector vectorA, Vector vectorB)
        {
            var resProduct = (vectorA.PositionX * vectorB.PositionX) + (vectorA.PositionY * vectorB.PositionY);
            return resProduct;
        }

        /// <summary>
        /// Method That calculates the distance between two vectors
        /// </summary>
        /// <param name="vectorA">First is a Vector</param>
        /// <param name="vectorB">Second is a Vector</param>
        /// <returns>A Double of the operation</returns>
        public double Distance(Vector vectorA, Vector vectorB)
        {
            var resDistance = Math.Pow(vectorB.PositionX - vectorA.PositionX, 2) + Math.Pow(vectorB.PositionY - vectorA.PositionY, 2);
            return Math.Sqrt(resDistance);
        }

        /// <summary>
        /// Method That calculates the length of a vector
        /// </summary>
        /// <param name="vector">First is a Vector</param>
        /// <returns>A Double of the operation</returns>
        public double Length(Vector vector)
        {
            var resLength = Math.Pow(vector.PositionX, 2) + Math.Pow(vector.PositionY, 2);
            return Math.Sqrt(resLength);
        }
    }
}
