﻿namespace Fundacion.Jala.Vector.BusinessLogic
{
    using Fundacion.Jala.Vector.BusinessLogic.Model;

    public class Vector : IVector
    {
        public Vector(int x, int y)
        {
            this.PositionX = x;
            this.PositionY = y;
        }

        public int PositionX { get; set; }

        public int PositionY { get; set; }

        public override string ToString()
        {
            return $"[{this.PositionX},{this.PositionY}]";
        }
    }
}
