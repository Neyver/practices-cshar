﻿namespace Fundacion.Jala.Vector.BusinessLogic.Model
{
    public interface IVector
    {
        int PositionX { get; set; }

        int PositionY { get; set; }

    }
}
