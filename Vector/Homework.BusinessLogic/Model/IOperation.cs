﻿namespace Fundacion.Jala.Vector.BusinessLogic.Model
{
    using System;

    public interface IOperation
    {
        Vector Add(Vector vectorA, Vector vectorB);

        int DotProduct(Vector vectorA, Vector vectorB);

        double Distance(Vector vectorA, Vector vectorB);

        double Length(Vector vector);
    }
}
