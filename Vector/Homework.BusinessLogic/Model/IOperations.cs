﻿namespace Homework.BusinessLogic.Model
{
    using System;

    public interface IOperations
    {
        void ShowArray(int[] array);

        int[] SumArray(int[] array, int[] array2);

        int[] ConvertArray(string value);
    }
}
