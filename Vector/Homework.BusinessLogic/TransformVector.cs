﻿namespace Fundacion.Jala.Vector.BusinessLogic
{
    using System;

    public class TransformVector
    {
        private const string InvalidArgumentMessage = "Argument are not valid, size 2";

        public TransformVector(string value)
        {
            string[] splitted = value.Split(',');

            if (splitted.Length == 2)
            {
                Vector = new Vector(int.Parse(splitted[0]), int.Parse(splitted[1]));
            }
            else
            {
                throw new ArgumentException(InvalidArgumentMessage);
            }
        }

        public Vector Vector { get; private set; }
    }
}
