﻿namespace Fundacion.Jala.Vector.Service
{
    using System;
    using Fundacion.Jala.Vector.BusinessLogic;
    using Fundacion.Jala.Vector.BusinessLogic.Model;

    public class Program
    {
        public static void Main(string[] args)
        {
            
            try
            {
                var vectorA = new TransformVector(args[0]);
                var vectorB = new TransformVector(args[1]);
                IOperation operation = new VectorOperations();
                var resSum = operation.Add(vectorA.Vector, vectorB.Vector);
                Console.WriteLine($"The Add :{resSum.ToString()}");

                var resDotProduct = operation.DotProduct(vectorA.Vector, vectorB.Vector);
                Console.WriteLine($"The DotProduct :{resDotProduct}");

                var resLength = operation.Length(vectorA.Vector);
                Console.WriteLine($"length vector A :{resLength}");

                resLength = operation.Length(vectorB.Vector);
                Console.WriteLine($"length vector B :{resLength}");

                var resDistance = operation.Distance(vectorA.Vector, vectorB.Vector);
                Console.WriteLine($"Distance between 2 point : {resDistance}");

            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
    }
}
